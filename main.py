# HANGMAN!!!!!! YAY!
# i missed a bunch of your zoom meetings because the dates on the show my homework posts were confusing and i tried joining the meetings a week late because of that

#also i couldn't be bothered to watch the entire youtube video so i just created a game of hangman
from random import randint
import os
  
file = open("listofwords.txt",)
words=[]
for line in file:
  words.append(line.rstrip())
file.close()
print(f"{len(words)} words loaded")

hangman_pictures = []
hangman_pictures.append(""" 
      _ _ _ _
     |
     |
     |
_____|_____

""")

hangman_pictures.append(""" 
      _ _ _ _
     |      O
     |
     |
_____|_____

""")
hangman_pictures.append( """ 
      _ _ _ _
     |      O
     |      |
     |
_____|_____

""")
hangman_pictures.append( """ 
      _ _ _ _
     |      O
     |    - |
     |
_____|_____

""")
hangman_pictures.append(""" 
      _ _ _ _
     |      O
     |    - | -
     |
_____|_____

""")
hangman_pictures.append(""" 
      _ _ _ _
     |      O
     |    - | -
     |     /
_____|_____

""")
hangman_pictures.append(""" 
      _ _ _ _
     |      O
     |    - | -
     |     / \\
_____|_____

""")
#print(len(hangman_pictures))

def get_indice(correct_word,guess):
   return  [i for i, a in enumerate(correct_word) if a == guess]



potato = randint(0,len(words))
#print(potato)
correct_word = words[potato]
#print(correct_word)

guessing_word = "-"*len(correct_word)
alreadyguessed = []

wrong_guesses = 0
lemon = 0
win = False
message = "Welcome to hangman"

while wrong_guesses < len(hangman_pictures) and not win:
  os.system('clear')
  print("Hangman")
  print(f"Hangman says: {message}")
  print(hangman_pictures[wrong_guesses])
  print(guessing_word)
  if len(alreadyguessed) != 0:
    print("wrong guesses so far:")
    print(alreadyguessed)

  player_input = input("guess a letter\n")
  
  # ensure correct player input
  if len(player_input) == 0:
    message = "you must enter something"
    continue
  elif player_input == correct_word:
    message = "you guessed the right word"
    win = True
    continue
  elif len(player_input) > 1:
    message = "since you entered multiple letters we will use your first letter"
    player_input = player_input[0]
  
  indices = get_indice(correct_word,player_input)
  if len(indices) == 0: #wrong answer
    alreadyguessed.append(player_input)
    message = "wrong guess"
    wrong_guesses+= 1
  else: # correct guess
    print("yay")
    list_ = list(guessing_word)
    for i in indices:
      list_[i] =  player_input
    guessing_word = "".join(list_)

  if guessing_word == correct_word:
    win = True
  
  
if win:
  print("you win")
else:
  print("you lose")
  print(f"the correct word was : {correct_word}")

    